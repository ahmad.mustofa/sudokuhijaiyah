package suhi.mesin;

import android.util.Log;

import com.sudokuhijaiyah.puzzle;

public class superSolution {

    //    String kalimat[] = {"البي", "خيرا", "جنوب", "اويون", "مالك", "جنة", "يوم", "ناكم", "باب"};
    int tmpS[][] = new int[9][9];
    int tmpS2[][] = new int[9][9];
    static int S[][] = new int[9][9];
    public static final String TAG = "print";

//    int S[][] = {{0, 7, 0, 0, 4, 0, 0, 0, 0},
//            {0, 9, 6, 5, 0, 0, 0, 0, 1},
//            {0, 0, 0, 1, 7, 3, 9, 0, 0},
//            {2, 0, 7, 0, 0, 0, 0, 0, 5},
//            {9, 5, 0, 0, 2, 0, 0, 6, 7},
//            {6, 0, 0, 0, 0, 0, 8, 0, 2},
//            {0, 0, 1, 2, 9, 8, 0, 0, 0},
//            {8, 0, 0, 0, 0, 5, 2, 7, 0},
//            {0, 0, 0, 0, 6, 0, 0, 3, 0}};

    public void setS(int[][] s) {
        S = s;
    }

    public void setSoal(int[][] Puzzle) {
        try {
            for (int i = 0; i < Puzzle.length; i++) {
                for (int j = 0; j < Puzzle[0].length; j++) {
                    S[i][j] = Puzzle[i][j];
                }
            }
            printSoal();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printSoal() {
        System.out.println("#################");
        System.out.println("Soal SuperSolution");
        for (int baris = 0; baris < 9; baris++) {
            for (int kolom = 0; kolom < 9; kolom++) {
//                Log.d(TAG, String.valueOf(S[kolom][baris]));
                System.out.print(S[kolom][baris]);
            }
//            Log.d(TAG, "\n");
            System.out.println();
        }
        System.out.println("#################");
    }

    public int[][] solusiSS() {
        solusi();
        return S;
    }

    public void solusi() {

        //masukkan indeks soal ke dalam variabel tmpS
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                tmpS[x][y] = S[x][y];
            }
        }

        // print indeks soal sesuai string kalimat
//        for (int x = 0; x < 9; x++) {
//            for (int y = 0; y < 9; y++) {
//                if (tmpS[x][y] == 0) {
//                    System.out.print("\t....");
//                } else {
//                    System.out.print("\t" + kalimat[(tmpS[x][y]) - 1]);
//                }
//            }
//            System.out.println("");
//        }

        //print indeks soal di variabel tmpS
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
//                Log.d(TAG, String.valueOf(S[x][y]));
//                Log.d(TAG, "\n");

                System.out.print(" " + tmpS[x][y]);
            }
            System.out.println();
        }

        //proses penyelesaian
        int iterasi = 0;
        int a = 0;
        int nilai = 1;

        while (a == 0 && iterasi <450) {

//            Log.d(TAG, "=========================");
//            Log.d(TAG, "Nilai = " + nilai);
//            Log.d(TAG, "Hitung untuk mendapatkan nilai 100");

            System.out.println("===========================");
            System.out.println("Nilai = " + nilai);
            System.out.println("Hitung untuk mendapatkan nilai 100");
            for (int baris = 0; baris < 9; baris++) {
                for (int kolom = 0; kolom < 9; kolom++) {
                    int angkaH = 0;
                    int angkaV = 0;
                    int angkaB = 0;
                    if (tmpS[baris][kolom] == 0) {
                        for (int z = 0; z < 9; z++) {
                            if (tmpS[baris][z] == nilai) {
                                angkaH += 1;
                            } else {
                                angkaH += 0;
                            }
                            if (tmpS[z][kolom] == nilai) {
                                angkaV += 1;
                            } else {
                                angkaV += 0;
                            }
                        }

                        //blok area 1
                        /*
                        100
                        000
                        000
                        */
                        if (baris < 3 & kolom < 3) {
                            for (int k = 0; k < 3; k++) {
                                for (int l = 0; l < 3; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 2
                        /*
                        O10
                        000
                        000
                        */
                        if (baris < 3 & kolom < 6 & kolom >= 3) {
                            for (int k = 0; k < 3; k++) {
                                for (int l = 3; l < 6; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB = 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 3
                        /*
                        O01
                        000
                        000
                        */
                        if (baris < 3 & kolom < 9 & kolom >= 6) {
                            for (int k = 0; k < 3; k++) {
                                for (int l = 6; l < 9; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 4
                        /*
                        O00
                        100
                        000
                        */
                        if (baris < 6 & baris >= 3 & kolom < 3) {
                            for (int k = 3; k < 6; k++) {
                                for (int l = 0; l < 3; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 5
                        /*
                        O00
                        010
                        000
                        */
                        if (baris < 6 & baris >= 3 & kolom < 6 & kolom >= 3) {
                            for (int k = 3; k < 6; k++) {
                                for (int l = 3; l < 6; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 6
                        /*
                        O00
                        001
                        000
                        */
                        if (baris < 6 & baris >= 3 & kolom < 9 & kolom >= 6) {
                            for (int k = 3; k < 6; k++) {
                                for (int l = 6; l < 9; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 7
                        /*
                        O00
                        000
                        100
                        */
                        if (baris < 9 & baris >= 6 & kolom < 3) {
                            for (int k = 6; k < 9; k++) {
                                for (int l = 0; l < 3; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 8
                        /*
                        O00
                        000
                        010
                        */
                        if (baris < 9 & baris >= 6 & kolom < 6 & kolom >= 3) {
                            for (int k = 6; k < 9; k++) {
                                for (int l = 3; l < 6; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }

                        //blok area 9
                        /*
                        O00
                        000
                        001
                        */
                        if (baris < 9 & baris >= 6 & kolom < 9 & kolom >= 6) {
                            for (int k = 6; k < 9; k++) {
                                for (int l = 6; l < 9; l++) {
                                    if (tmpS[k][l] == nilai) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (angkaH != 1) {
                            angkaH = 0;
                        } else {
                            angkaH = 5;
                        }
                        if (angkaV != 1) {
                            angkaV = 0;
                        } else {
                            angkaV = 5;
                        }
                        if (angkaB != 1) {
                            angkaB = 0;
                        } else {
                            angkaB = 5;
                        }

                        tmpS[baris][kolom] = (100 - (angkaH + angkaV + angkaB));
                    }
//                    Log.d(TAG, "" + tmpS[baris][kolom]);
                    System.out.print(" " + tmpS[baris][kolom]);
                }
                System.out.println();
            }

//            Log.d(TAG, "===============================");
//            Log.d(TAG, "Hitung Nilai yang = 100");

            System.out.println("============================");
            System.out.println("Hitung Nilai yang == 100");
            int nilai100 = 100;
            for (int m = 0; m < 9; m++) {
                for (int n = 0; n < 9; n++) {
                    int angkaH = 0;
                    int angkaV = 0;
                    int angkaB = 0;
                    if (tmpS[m][n] == nilai100) {
                        for (int o = 0; o < 9; o++) {
                            if (tmpS[m][o] == nilai100) {
                                angkaH += 1;
                            } else {
                                angkaH += 0;
                            }
                            if (tmpS[o][n] == nilai100) {
                                angkaV += 1;
                            } else {
                                angkaV += 0;
                            }
                        }
                        if (m < 3 & n < 3) {
                            for (int k = 0; k < 3; k++) {
                                for (int l = 0; l < 3; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 3 & n < 6 & n >= 3) {
                            for (int k = 0; k < 3; k++) {
                                for (int l = 3; l < 6; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 3 & n < 9 & n >= 6) {
                            for (int k = 0; k < 3; k++) {
                                for (int l = 6; l < 9; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 6 & m >= 3 & n < 3) {
                            for (int k = 3; k < 6; k++) {
                                for (int l = 0; l < 3; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 6 & m >= 3 & n < 6 & n >= 3) {
                            for (int k = 3; k < 6; k++) {
                                for (int l = 3; l < 6; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 6 & m >= 3 & n < 9 & n >= 6) {
                            for (int k = 3; k < 6; k++) {
                                for (int l = 6; l < 9; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 9 & m >= 6 & n < 3) {
                            for (int k = 6; k < 9; k++) {
                                for (int l = 0; l < 3; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 9 & m >= 6 & n < 6 & n >= 3) {
                            for (int k = 6; k < 9; k++) {
                                for (int l = 3; l < 6; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (m < 9 & m >= 6 & n < 9 & n >= 6) {
                            for (int k = 6; k < 9; k++) {
                                for (int l = 6; l < 9; l++) {
                                    if (tmpS[k][l] == nilai100) {
                                        angkaB += 1;
                                    } else {
                                        angkaB += 0;
                                    }
                                }
                            }
                        }
                        if (angkaH != 1) {
                            angkaH = 0;
                        } else {
                            angkaH = 5;
                        }
                        if (angkaV != 1) {
                            angkaV = 0;
                        } else {
                            angkaV = 5;
                        }
                        if (angkaB != 1) {
                            angkaB = 0;
                        } else {
                            angkaB = 5;
                        }

                        if ((100 + (angkaH + angkaV + angkaB)) > 100) {
                            tmpS2[m][n] = nilai;
                        }
                        if ((100 + (angkaH + angkaV + angkaB)) == 100) {
                            tmpS2[m][n] = 0;
                        }
                        System.out.print(" " + (100 + (angkaH + angkaV + angkaB)));
//                        Log.d(TAG, "" + (100 + (angkaH + angkaV + angkaB)));

                    } else {
                        if (tmpS[m][n] <= 100 & tmpS[m][n] > 9) {
                            tmpS2[m][n] = 0;
                        } else {
                            tmpS2[m][n] = tmpS[m][n];
                        }
                        System.out.print(" " + tmpS[m][n]);
//                        Log.d(TAG, "" + tmpS[m][n]);
                    }
                }
                System.out.println("");
            }
            for (int x = 0; x < 9; x++) {
                for (int y = 0; y < 9; y++) {
                    tmpS[x][y] = tmpS2[x][y];
                }
            }

            int ada = 0;
            for (int x = 0; x < 9; x++) {
                for (int y = 0; y < 9; y++) {
                    if (tmpS[x][y] == 0) {
                        ada += 1;
                    }
                }
            }

            if (ada != 0) {
                a = 0;
                if (nilai < 9) {
                    //update nilai untuk pencarian angka selanjutnya (2-9)
                    nilai += 1;
                } else {
                    nilai = 1;
                }

                //update iterasi
            } else {
                a = 1;
                //kirim status penyelesain SS sempurna
                //codenya disini
            }

//                System.out.println("#########################");
//                System.out.println("ITERASI KE = " + iterasi);
//                System.out.println("#########################");

            iterasi += 1;
            Log.i(TAG, "iterasi ke = " + iterasi);
        }
        //End of While
        Log.i(TAG, "End of iterasi");

        System.out.println("================================");
//        Log.d(TAG, "==========================");

        //update soal selesai di array tmpS2 ke array S
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                S[x][y] = tmpS2[x][y];
            }
        }

        //tampil indeks soal selesai
        System.out.println("Indeks Soal Selesai" + "\n" + "==================");
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                System.out.print(" " + S[x][y]);
//                Log.d(TAG, "" + S[x][y]);
            }
            System.out.println("");
        }
        //tampil kalimat
//        System.out.println("==============================");
//        for (int x = 0; x < 9; x++) {
//            for (int y = 0; y < 9; y++) {
//                System.out.print("\t" + kalimat[(S[x][y]) - 1]);
//            }
//            System.out.println("");
//        }
    }

}

